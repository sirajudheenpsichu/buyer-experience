/**
 * Utility function to parse a string into kebab-case
 *
 * @param {String} str - A String to be converted to kebab-case
 */
const toKebabCase = (str) =>
  str &&
  str
    .match(/[A-Z]{2,}(?=[A-Z][a-z]+[0-9]*|\b)|[A-Z]?[a-z]+[0-9]*|[A-Z]|[0-9]+/g)
    .map((x) => x.toLowerCase())
    .join('-');

module.exports = { toKebabCase };
